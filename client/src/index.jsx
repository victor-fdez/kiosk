import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import KioskApp from './kiosk/reducers/'
import Kiosk from './kiosk'
import { initConnection, startConnection } from './kiosk/actions/connection'

// create the redux store
let store = createStore(
  KioskApp,
  //
  applyMiddleware(thunk, logger) 
);

let socket_url = `${HOST}:${PORT}`
let url = new URL(window.location.href)
let mac = url.searchParams.get('mac')
if (mac == null)
  mac = 'FF:FF:FF:FF:FF:FF' 

store.dispatch(initConnection(socket_url, mac));
store.dispatch(startConnection());

render(
  <Provider store={store}>
    <Kiosk />
  </Provider>,
  document.getElementById('app')
)

