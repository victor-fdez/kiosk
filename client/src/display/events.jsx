import React from 'react'
import PropTypes from 'prop-types'
import 'bootstrap/js/dist/carousel'
import './events.scss'
import moment from 'moment'
import deepEqual from 'deep-equal'
import 'fullcalendar/dist/fullcalendar'
import 'fullcalendar/dist/fullcalendar.css'
//
const dxcImgStyle={
  display: 'block',
  margin: '5px auto'
}

class Events extends React.Component {
  componentDidMount(){
    // transform events to add them into full calendar
    const events = this.props.events.map((event) => {
      return {
        title: event.name,
        start: event.start,
        end: event.end
      }
    });
    $('.carousel').carousel({
      interval: 5000
    })
    $('#calendar').fullCalendar({
      // put your options and callbacks here
      header: {
        left: '',
        center: 'title',
        right: ''
      },
      events: events, 
      displayEventTime : true,
      displayEventEnd: false,
      eventColor: 'yellow',
      eventTextColor: 'black',
      height: "auto"
    })
  }
  shouldComponentUpdate(nextProps){
    if (!deepEqual(this.props.events, nextProps.events)){
      return true;
    } else {
      return false;
    }
  }
  componentWillUpdate(){
    $('.carousel').carousel('pause');
    $('.carousel').carousel('dispose');
  }
  componentDidUpdate(){
    const events = this.props.events.map((event) => {
      return {
        title: event.name,
        start: event.start,
        end: event.end
      }
    })
    $('#calendar').fullCalendar('removeEvents')
    $('#calendar').fullCalendar('addEventSource', events)
    $('.carousel').carousel({
      interval: 5000
    })
    $('.carousel').carousel(2*events.length)
  }
  render(){
    // flatten out the events to slides
    const slides = this.props.events.reduce((arr, event) => {
      arr.push(
        // add event to slides
        {
          type: 'event',
          obj: event
        },
        // add presenter to slides
        {
          type: 'presenter',
          obj: { event_id: event.id, ...event.presenter}
        }
      );
      return arr;
    }, []);
    const host = this.props.host;
    return (
      <div>
        <div className="container-fluid">
          <div className="carousel slide" data-ride="carousel">
            <div className="carousel-inner">
              {
                slides.map((slide, index) => {
                  const type = slide.type;
                  if(type=='event'){
                    const event = slide.obj;
                    return (
                      <div key={type+event.id} className="carousel-item" style={{ marginBottom: '50px' }}>
                        <div className='container-fluid'>
                          <div className="row">
                            <div className="col">
                              <div className="row">
                                <h2 className="display-3">{ event.name }</h2>
                              </div>
                              <div className="row">
                                <h2>{ event.location }</h2>
                              </div>
                              <div className="row">
                                <h2>{ moment(event.start).calendar(null, { sameElse: 'DD/MM/YYYY hh:mm a'})}</h2>
                              </div>
                              <div className="row" style={{marginTop: '2%'}}>
                                <p className="display-5 grey-text">{ event.description }</p>
                              </div>
                            </div>
                            <div className="col">
                              <img src={"http://"+host+event.image} height="500" className={"rounded float-right"} alt="Responsive image" />
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  } else {
                    const presenter = slide.obj;
                    return (
                      <div key={type+presenter.id+","+presenter.event_id} className="carousel-item">
                        <div className='container-fluid'>
                          <div className="row">
                            <div className="col">
                              <div className="row">
                                <h1 className="display-1">{ presenter.name }</h1>
                              </div>
                              <div className="row">
                                <h2 className="display-3 mb-2 text-muted">{ presenter.position }</h2>
                              </div>                
                              <div className="row">
                                <p className="display-5 grey-text">{ presenter.description }</p>
                              </div>
                            </div>
                            <div className="col">
                              <img src={"http://"+host+presenter.image} height="500" className={"rounded float-right"} alt="Responsive image" />
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  }
                })
              }
              {/* calendar with all the events */}
              <div className="carousel-item active">
                <div className="container">
                  <div id='calendar'> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* navbar containing dxc logo */}
        <nav className="navbar fixed-bottom navbar-light bg-white">
            <img height="50" style={dxcImgStyle} className={"rounded mx-auto"} src={"http://"+host+"/static/slides/dxclogo.png"} />
        </nav>
      </div>
    );
  }
};

Events.propTypes = {
  events: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired
    })    
  )
};

export default Events;
