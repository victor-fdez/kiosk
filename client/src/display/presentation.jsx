import React from 'react';
import PropTypes from 'prop-types';

class Presentation extends React.Component {
  render({presentation}){
    return (
      <div></div>
    );
  }
};

Presentation.propTypes = {
  presentation: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string.isRequired   
    })
  )
};

export default Presentation;

