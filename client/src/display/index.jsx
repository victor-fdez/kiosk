import React from 'react';
import PropTypes from 'prop-types';
import Presentation from './presentation';
import Events from './events';
import './index.scss'

const Display = ({connection, location, presentation, events}) => {
    return (
      <div className="container-fluid">
        {(!presentation && !events) ? (
          <div className="row  align-items-center justify-content-center">
            <div className="col"></div>
            <div className="col-3">
              <div className="col-12">Connected: {""+connection.isConnected}</div>
              <div className="col-12">Host: {connection.host}</div>
              <div className="col-12">MAC: {connection.mac}</div>
              <div className="col-12">State: {connection.state}</div>
              <div className="col-12">Description: {connection.description}</div>
              {(location) ? (
                <div className="col-12">
                  <div className="row">
                    <div className="col-12" >has been assigned location </div>
                    <div className="col-12" >name: {location.name} </div>
                    <div className="col-12" >code: {location.building_code} </div>
                    <div className="col-12" >description: {location.building_description} </div>
                  </div>
                </div>
              ):(
                <div className="col" >Location: No location has been assigned</div>
              )}
            </div>
            <div className="col"></div>
          </div>
        ) : (<div></div>)}
        {(presentation && !events) ? (
          <Presentation presentation={presentation} />
        ) : (<div></div>)}
        {(!presentation && events) ? (
          <Events events={events} host={connection.host} />
        ) : (<div></div>)}
      </div>
    );
}; 

Display.propTypes = {
  // information regarding connection
  connection: PropTypes.shape({
    isConnected: PropTypes.bool.isRequired,
    host: PropTypes.string.isRequired,
    mac: PropTypes.string.isRequired,
    description: PropTypes.string
  }),
  // information regarding location of device
  location: PropTypes.shape({
    building: PropTypes.string,
    name: PropTypes.string
  }),
  presentation: PropTypes.array,
  events: PropTypes.array
}

export default Display;
