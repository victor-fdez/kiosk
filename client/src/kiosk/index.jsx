import { connect } from 'react-redux'
import React from 'react';
import ReactDOM from 'react-dom';
import Display from './../display';

const mapStateToProps = (state, ownProps) => {
  return {
    ...state
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
}

const Kiosk = connect(
  mapStateToProps,
  mapDispatchToProps
)(Display);

export default Kiosk;
