import {Actions} from '../consts'
import has from 'lodash/has'
import get from 'lodash/get'

const messages = {
};

function connecting(){
  return {
    type: Actions.CONNECT
  }
}

function connected(){
  return {
    type: Actions.CONNECTED
  }
}

function disconnected(){
  return {
    type: Actions.DISCONNECTED
  }
}

function kioskDataUpdate(data){
  return {
    type: Actions.KIOSK_DATA_UPDATE,
    payload: {
      ...data
    }
  };
}

function connectionFailed(){
  return (dispatch, getState) => {
    dispatch(kioskDataUpdate({}))
    dispatch(disconnected())
  }
}

export function initConnection(host, mac){
  return {
    type: Actions.INIT,
    payload: {
      host,
      mac 
    }
  };
}

export function sentPing(){
  return {
    type: Actions.PING
  }
}

export function gotPong(){
  return {
    type: Actions.PONG
  }
}

export function pongFailed(){
  return {
    type: Actions.PONG_FAILED
  }
}

export function startConnection(){
  return (dispatch, getState) => {
    const state = getState();
    const host = state.connection.host; 
    const mac = state.connection.mac; 
    let pingInfo = {
      tm: null
    };
    // functions to ping/pong server to maintain connection
    const ping = (conn) => {
      conn.send(JSON.stringify({type: 'ping', payload: { mac: mac }}))
      dispatch(sentPing())
      pingInfo.tm = setTimeout(() => {
        // close current connection and reconnect
        dispatch(pongFailed())
        conn.close()
        dispatch(startConnection())
      }, 10000)
    };
    const pong = () => {
      dispatch(gotPong())
      clearTimeout(pingInfo.tm)
    };
    dispatch(connecting());
    return new Promise((resolve, reject) => {
      var sock = new WebSocket(`ws://${host}/kiosk/${mac}/`);
      sock.onopen = (event) => {
        resolve()
        ping(sock)
      };
      // setup communication functions
      sock.onmessage = (msg) => {
        if (msg.data == '__pong__') {
          pong()
          // send ping again...
          setTimeout(() => { ping(sock) }, 10000)
          return;
        }
        let data = JSON.parse(msg.data);
        dispatch(kioskDataUpdate(data));
      };
      sock.onclose = () => {
        reject();
      };
    }).then(
      () => { 
        // if the connection succeeds
        dispatch(connected()); 
      },
      (error) => { 
        // if we fail
        dispatch(connectionFailed()); 
        // wait 10 seconds and retry
        setTimeout(() => {
          dispatch(startConnection());
        }, 10000);
      }
    );
  }
};




