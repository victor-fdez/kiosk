import { Actions, ConnectionStates } from '../consts';
import has from 'lodash/has'
import isArray from 'lodash/isArray'

const events = (state = null, action = {type: undefined }) => {
  switch(action.type) {
    case Actions.KIOSK_DATA_UPDATE:
      if (has(action, 'payload.events') && isArray(action.payload.events)){
        return action.payload.events.slice();  
      }
      return null;
    default: 
      return state;
  }
};

export default events;
