import { combineReducers } from 'redux'
import connection from './connection'
import location from './location'
import events from './events'

const KioskApp = combineReducers({
  connection,
  location,
  events
})

export default KioskApp
