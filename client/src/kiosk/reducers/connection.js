import { Actions, ConnectionStates } from '../consts';

const connection = (state = {}, action = {type: undefined }) => {
  switch(action.type) {
    case Actions.CONNECT:
      return Object.assign({}, state, {
        state: ConnectionStates.RECONNECTING,
        isConnected: false 
      });
    case Actions.CONNECTED:
      return Object.assign({}, state, {
        state: ConnectionStates.CONNECTED,
        isConnected: true
      });
    case Actions.DISCONNECTED:
      return Object.assign({}, state, {
        state: ConnectionStates.DISCONNECTED,
        isConnected: false 
      });
    case Actions.KIOSK_DATA_UPDATE:
      return Object.assign({}, state, {
        state: ConnectionStates.CONNECTED,
        description: action.payload.description
      });
    case Actions.INIT:
      return {
        state: ConnectionStates.DISCONNECTED,
        isConnected: false,
        host: action.payload.host,
        mac: action.payload.mac
      };
    default: 
      return Object.assign({}, state);
  }
};



export default connection;
