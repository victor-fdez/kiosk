import { Actions, ConnectionStates } from '../consts';
import has from 'lodash/has'

const location = (state = null, action = {type: undefined }) => {
  switch(action.type) {
    case Actions.KIOSK_DATA_UPDATE:
      if (has(action.payload,'location')){
        return Object.assign({}, action.payload.location);  
      }
      return null;
    default: 
      return state;
  }
};

export default location;

