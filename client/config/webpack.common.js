const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const basedir = path.resolve(__dirname,'../');

module.exports = {
  entry: [
    path.resolve(basedir,'src','index.jsx')
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader', 'eslint-loader']
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader"
        }) 
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [{
            loader: "css-loader"
          }, {
            loader: "sass-loader"
          }]
        }) 
      },
      {
         test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
         use: [
           'file-loader'
         ]
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  plugins: [
    new CleanWebpackPlugin(['dist/*'], { root: path.resolve(basedir), verbose: true }),
    new webpack.DefinePlugin({
      HOST: process.env['KIOSK_ADMIN_HOST'] ? JSON.stringify(process.env['KIOSK_ADMIN_HOST']) : 'localhost',
      PORT: process.env['KIOSK_ADMIN_PORT'] ? JSON.stringify(process.env['KIOSK_ADMIN_PORT']) : '8080',
      NODE_ENV: process.env['NODE_ENV'] ? JSON.stringify(process.env['NODE_ENV']) : 'development'
    }),
    new HtmlWebpackPlugin({
      title: 'Kiosk',
      template: 'src/index.ejs'
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default'],
      // In case you imported plugins individually, you must also require them here:
      Util: "exports-loader?Util!bootstrap/js/dist/util",
      Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
      Carousel: "exports-loader?Carousel!bootstrap/js/dist/carousel"
    }),
    // load all css styles as this file
    new ExtractTextPlugin('styles/main.[hash].css', {
        allChunks: true
    })
  ],
  output: {
    path: path.resolve(basedir, 'dist'),
    filename: 'bundle.[hash].js'
  }
}
