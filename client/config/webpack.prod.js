const path = require('path');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const common = require('./webpack.common.js');
const basedir = path.resolve(__dirname,'../');

module.exports = merge(common, {
  devtool: 'cheap-source-map',
  plugins: [
    new UglifyJSPlugin({
      parallel: true,
      uglifyOptions: {
        output: {
          comments: false,
          beautify: false,
        },
        compress: true
      }
    }),
  ],
});
