const path = require('path');
const merge = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const common = require('./webpack.common.js');
const basedir = path.resolve(__dirname,'../');

module.exports = merge(common, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(basedir,'dist')
  },
  plugins: [
    new CopyWebpackPlugin([{ from: 'assets/test/**', to: 'assets/', flatten: true }]),  
  ]
});
