#!/bin/bash
set -e

# generate static assets
./manage.py collectstatic --clear --no-input
# wait for postgres
./wait-pg.sh
# run program 
./manage.py migrate
./manage.py runserver 0:8080
