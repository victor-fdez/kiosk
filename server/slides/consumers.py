import json
import logging
from channels import Group
from django.core.exceptions import ObjectDoesNotExist
from pydash.objects import has, get
from slides.models import Kiosk

logger = logging.getLogger("django")
def add_device(message, mac_device):
    kiosk=None
    try:
        kiosk=Kiosk.objects.get(mac_address__exact=f"{mac_device}")
    except ObjectDoesNotExist:
        kiosk=Kiosk(mac_address=mac_device,description="raspberry pi3", location=None)
        kiosk.save()
    Group(kiosk.group()).add(message.reply_channel)
    return kiosk

# Connected to websocket.connect
def ws_add(message, mac_device):
    # Accept the incoming connection
    logger.info(f"device with {mac_device} connected")
    message.reply_channel.send({"accept": True })
    kiosk = add_device(message, mac_device)
    kiosk.broadcast()

# Connected to websocket.receive
def ws_message(message):
    text = message.content['text']
    msg = json.loads(text)
    if get(msg, 'type') == 'ping':
        if has(msg, 'payload.mac'):
            mac = get(msg, 'payload.mac')
            kiosk = add_device(message, mac)
            kiosk.set_connected()
        message.reply_channel.send({"text": "__pong__" })

# Connected to websocket.disconnect
def ws_disconnect(message):
    Group("chat").discard(message.reply_channel)


