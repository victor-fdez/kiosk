# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from slides.models import Event, Presenter, Location, Kiosk, PresentationGroup

class EventAdmin(admin.ModelAdmin):
    list_display = ['name', 'start', 'presenter']

class PresenterAdmin(admin.ModelAdmin):
    list_display = ['name', 'position', 'contact_email']

def update_kiosk(modeladmin, request, queryset):
    print('update kiosks')
update_kiosk.short_description = "Update software"

class KioskAdmin(admin.ModelAdmin):
    list_display = ['mac_address', 'location', 'is_connected']
    ordering = ['mac_address']
    actions = [update_kiosk]

class LocationAdmin(admin.ModelAdmin):
    list_display = ['name', 'building_code']
    ordering = ['building_code']

class PresentationGroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'presentation_type']

#class PresentationAdmin(admin.ModelAdmin):
#    model = Presentation 
#    list_display = ['name', ]
#    inlines = [SlideInline]

admin.site.register(Event, EventAdmin)
admin.site.register(Presenter, PresenterAdmin)
admin.site.register(Kiosk, KioskAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(PresentationGroup, PresentationGroupAdmin)
