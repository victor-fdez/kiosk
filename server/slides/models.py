# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.utils import timezone
from django.dispatch import receiver
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.forms.models import model_to_dict
from pydash.objects import set_
from enumfields import EnumField, Enum
from django.core.serializers import serialize
from django.core.cache import cache
#from channels.binding.websockets import WebsocketBindingWithMembers
from channels import Group 


def update_presentation_groups():
    pgs = PresentationGroup.objects.filter(presentation_type=PresentationGroupType.EVENTS)
    for pg in pgs.all():
        for kiosk in pg.kiosk_set.all():
            Group(kiosk.group()).send({'text': json.dumps(kiosk.to_dict()) })

def upload_to_by_id(instance, filename):
    class_name=instance.__class__.__name__
    obj_id=instance.id
    return f"./{class_name}/{obj_id}/{filename}"

class Presenter(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    position = models.CharField(max_length=50)
    contact_email = models.EmailField(max_length=50)
    image = models.ImageField(upload_to=upload_to_by_id)
    def __str__(self):
        return '%s' % (self.name)

@receiver(post_save, sender=Presenter)
def presenter_update(sender, instance, **kwargs):
    update_presentation_groups()

@receiver(post_delete, sender=Presenter)
def presenter_delete(sender, instance, **kwargs):
    update_presentation_groups()

class Event(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    image = models.ImageField(upload_to=upload_to_by_id)
    presenter = models.ForeignKey(
        Presenter,
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return '%s' % (self.name)

@receiver(post_save, sender=Event)
def events_update(sender, instance, **kwargs):
    print('receive change')
    update_presentation_groups()

@receiver(post_delete, sender=Event)
def events_delete(sender, instance, **kwargs):
    update_presentation_groups()

class PresentationGroupType(Enum):
    EVENTS = 'e'
    PRESENTATION = 'p'

class PresentationGroup(models.Model):
    name = models.CharField(max_length=100)
    presentation_type = EnumField(PresentationGroupType, max_length=1)
    def __str__(self):
        return '%s-%s' % (self.name, self.presentation_type)

@receiver(post_save, sender=PresentationGroup)
def presentation_group_update(sender, instance, **kwargs):
    for kiosk in instance.kiosk_set.all():
        Group(kiosk.group()).send({'text': json.dumps(kiosk.to_dict()) })


class Location(models.Model):
    name = models.CharField(max_length=100)
    building_code = models.CharField(max_length=10)
    building_description = models.TextField()
    def __str__(self):
        return '%s-%s' % (self.building_code, self.name)

@receiver(post_save, sender=Location)
def location_update(sender, instance, **kwargs):
    for kiosk in instance.kiosk_set.all():
        Group(kiosk.group()).send({'text': json.dumps(kiosk.to_dict()) })

class Kiosk(models.Model):
    mac_address = models.CharField(max_length=18)
    description = models.TextField()
    location = models.ForeignKey(
            Location,
            on_delete=models.SET_NULL,
            blank=True,
            null=True)
    presentation_group = models.ForeignKey(
            PresentationGroup,
            on_delete=models.SET_NULL,
            blank=True,
            null=True)
    def group(self):
        mac_address=self.mac_address.replace(":","-")
        return f"kiosk.{mac_address}"
    def __str__(self):
        return 'kiosk.%s' % (self.mac_address)
    def to_dict(self):
        d_self = model_to_dict(self)
        location = self.location
        pg = self.presentation_group
        if location is not None:
            d_location = model_to_dict(location)
            set_(d_self, 'location', d_location)
        if pg is not None:
            if pg.presentation_type == PresentationGroupType.EVENTS:
                events=Event.objects.all()
                d_events=[]
                for event in events:
                    d_event=model_to_dict(event)
                    d_presenter=model_to_dict(event.presenter)
                    set_(d_event, 'start' , str(d_event['start']))
                    set_(d_event, 'end' , str(d_event['end']))
                    set_(d_event, 'image' , d_event['image'].url)
                    set_(d_event, 'presenter', d_presenter)
                    set_(d_event, 'presenter.image', d_presenter['image'].url)
                    d_events.append(d_event)
                set_(d_self, 'events', d_events)
            else:
                print('presentation')
        return d_self
    def broadcast(self):
        Group(self.group()).send({'text': json.dumps(self.to_dict()) })

    def is_connected(self):
        return cache.ttl(f"dxc.kiosk.{self.mac_address}") != 0 

    def set_connected(self):
        cache.set(f"dxc.kiosk.{self.mac_address}", 1, timeout=20)

@receiver(post_save, sender=Kiosk)
def kiosk_update(sender, instance, **kwargs):
    instance.broadcast()

