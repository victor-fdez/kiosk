from channels.routing import route
from slides.consumers import ws_add, ws_message, ws_disconnect

channel_routing = [
    route("websocket.connect", ws_add, path=r"^/kiosk/(?P<mac_device>[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2})/$"),
    route("websocket.receive", ws_message),
    route("websocket.disconnect", ws_disconnect)
]
