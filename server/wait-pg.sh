#!/bin/bash
set -e

# if the env variable is not defined then exit fail
if [ "$DATABASE_HOST" == "" ]; then
  exit 1;
fi

# wait until postgres is available
until pg_isready -h $DATABASE_HOST; do
  >&2 echo "postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "postgres is available"
