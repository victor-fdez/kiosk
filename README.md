# Kiosk

## Development

To develop this project you need to have the following software setup on your computer.

* [Docker](https://github.com/docker/compose/releases)
* [Docker Compose](https://www.docker.com/) under (get docker)

To start all of the containers for this project just run the following command.

```bash
# to build the project
docker-compose build
# to start all containers
docker-compose up 
```

### Containers

The main containers on this project are the following:

#### Proxy

This is an NGinx instance that proxies traffic to both the client and server instances. The configuration is contained within ```proxy/```.

```bash
docker-compose exec proxy bash
```

#### Admin

This is a Python [Django](https://www.djangoproject.com/) which contains the main information for all RPI devices. All RPI devices connect to this container, and get updates from it relating to the information they should be displaying.

```bash
docker-compose exec admin bash
# normal python/django commands here like ./manage.py
```

#### Client

This ia a JS [WebPack](https://webpack.js.org/) dev server that compiles the JS, SASS, and other assets to easily send them to the RPI devices for display on their web browsers.

```bash
docker-compose exec client bash
# normal nodejs/yarn commands here like npm and yarn 
```

#### PostGres DB

This is a DB that will hold all the information stored by the Admin container. 

```bash
docker-compose exec postgres bash
```
