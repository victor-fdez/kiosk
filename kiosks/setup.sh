#!/bin/bash

# Store server IP
SERVER_ADDR=$1
HTTP_PROXY=$2

# Copy startup script and update permissions
echo "##### Copying script"
cp ./startup.sh ~/startup.sh
chmod 744 ~/startup.sh
echo "##### Script ready"

# Update autostart file
echo "##### Setting up autostart"
sudo echo "@/home/pi/startup.sh ${SERVER_ADDR}" "${HTTP_PROXY}" >> ~/.config/lxsession/LXDE-pi/autostart
echo "##### Autostart ready"

# Disable sleep
echo "##### Disable sleep"
sudo sed -i '/#xserver-command=X/c\xserver-command=X -s 0 dpms' /etc/lightdm/lightdm.conf
echo "##### Sleep Disabled"

echo  "##### Rebooting..."
for i in {0..4}
do 
  echo $((5 - $i))
  sleep 1
done
sudo reboot
