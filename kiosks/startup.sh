#!/bin/bash

# Obtain URL values
SERVER_ADDR=$1
export http_proxy="$2"
MAC_ADDRESS=`ifconfig eth0 | grep ether | awk '{ print $2 }'`

# Wait for startup to "finish"
sleep 5

# Start browser in kiosk + incognito mode
chromium-browser --incognito --noerrdialogs --kiosk --proxy-server="$http_proxy" http://${SERVER_ADDR}/?mac=${MAC_ADDRESS}
